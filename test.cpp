#include "SerialPort.hpp"

unsigned char temp[6];
unsigned char data[1];
int n;
int it;

int main() {
	SerialPort serialPort("/dev/ttymxc4", BaudRate::B_115200);
	serialPort.SetTimeout(-1);
	serialPort.Open();

	while(1){
		// Lendo byte a byte
		n = serialPort.ReadBinary(data, 1);

		// Se houver algum byte...
		if(n != 0)
		{
			// Armazena no buffer de pacote
			temp[it] = data[0];
			it++;

			// Se o último byte recebido for um final de pacote
			if(data[0] == 0x55) {
				it=0;
				pressure = (temp[1] << 8) | temp[2];	// Duas primeiras posições é a pressão
				// TODO checar posição 3 e 4 para verificar se o checksum está correto
				
				printf("%d\n", pressure); // Exibe valor recebido
			}
		}
	}
}

